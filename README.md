# Quality Assurance on Statistical Operation Tools #
-------

Accumulated code project that can compute simple statistical operations with `.txt` or `.xlsx` files such as: 

* *correlations*,
* *intervals*,
* *regression*,
* *variances*.

The code wasn't evaluated. Instead, all necessary information about time, flaws, planning, etc. must be manually filled within different excel spreadsheets.

**Figure 1**: Project planification form

![330_1.png](https://bitbucket.org/repo/pG9k9L/images/2499521843-330_1.png)

**Figure 2**: Timestamp form

![330_2.png](https://bitbucket.org/repo/pG9k9L/images/3043162670-330_2.png)

# Objectives #
-------
* Learn how to document progress based on written procedures in a business context.
* Follow **procedures and walkthroughs.**
* Make a conclusion from gathered information and propose improvements for the next project.

# How do I get set up? #
-------
Run Configuration: `valeurs.txt correlation.txt regression.txt TP5.xlsx`

# For more information #
-------
Visit the following website: [**Software Quality Assurance** (LOG330)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=log330) [*fr*]