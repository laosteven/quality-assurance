import java.io.*;
import java.util.*;

import org.apache.poi.xssf.usermodel.*;

/***************************************************************
 * Nom: Steven Date: 2016-02-16 Description: Effectuer calculs
 ***************************************************************/
public class cLOG330Calculs
{
	private static final int XK = 900;
	private static final double CORR_PARFAIT = 0.9;
	private static final double CORR_FORTE = 0.7;
	private static final double CORR_MODERE = 0.5;
	private static final double CORR_FAIBLE = 0.3;

	private static List<Float> aXCoords;
	private static List<Float> aYCoords;

	// valeurs.txt correlation.txt regression.txt TP5.xlsx
	public static void main(String[] pArgs) throws NumberFormatException, IOException
	{
		if (pArgs != null && pArgs.length > 0)
		{
			float[] reg = new float[3];
			mCalculateVariance(pArgs[0]);
			mCalculateCorrelation(pArgs[1], false);
			reg = mCalculateRegression(pArgs[2]);
			mCalculateCorrelation(pArgs[3], true);
			mCalculateInterval(reg);
		}
		else
		{
			System.out.println("Fichiers non definis");
		}
	}

	private static void mCalculateInterval(float[] reg)
	{
		float lPente = reg[0];
		float lCste = reg[1];
		float lXMoy = reg[2];

		float lVar = mCalcSumVar(aXCoords, aYCoords, lPente, lCste);
		float lEcType = (float) Math.sqrt(lVar);
		float lY = lPente * XK + lCste;

		// 90
		float lItvl90 = mCalcInterval((float) 1.860, lEcType, lXMoy);
		float lInf90 = lY - lItvl90;
		float lSup90 = lY + lItvl90;

		// 70
		float lItvl70 = mCalcInterval((float) 1.108, lEcType, lXMoy);
		float lInf70 = lY - lItvl70;
		float lSup70 = lY + lItvl70;

		mDisplayIntervalle((int) lY, lItvl90, lInf90, lSup90, lItvl70, lInf70, lSup70);
	}

	private static float[] mCalculateRegression(String pArgs) throws FileNotFoundException, IOException
	{
		aXCoords = new ArrayList<Float>();
		aYCoords = new ArrayList<Float>();

		mLectureTxt(pArgs, aXCoords, aYCoords);

		float lXSum = mCalcSum(aXCoords, 1);
		float lYSum = mCalcSum(aYCoords, 1);
		float lXYSum = mCalcSum(aXCoords, aYCoords);

		float lXSquaredSum = mCalcSum(aXCoords, 2);

		float lYMoy = (float) mCalcMoyenne(aYCoords);
		float lXMoy = (float) mCalcMoyenne(aXCoords);

		float lPenteNum = lXSum * lYSum - aXCoords.size() * lXYSum;
		float lPenteDen = (float) (Math.pow(lXSum, 2) - aXCoords.size() * lXSquaredSum);

		float lPente = lPenteNum / lPenteDen;
		float lConstante = lYMoy - lPente * lXMoy;

		mDisplayRegression(aXCoords, aYCoords, lPente, lConstante);

		float[] reg = { lPente, lConstante, lXMoy };
		return reg;
	}

	private static void mCalculateCorrelation(String pArgs, boolean pIsExcel) throws FileNotFoundException, IOException
	{
		List<Float> lXCoords = new ArrayList<Float>();
		List<Float> lYCoords = new ArrayList<Float>();

		if (pIsExcel)
		{
			mLectureExcel(pArgs, lXCoords, lYCoords);
		}
		else
		{
			mLectureTxt(pArgs, lXCoords, lYCoords);
		}

		float lXYSum = mCalcSum(lXCoords, lYCoords);
		float lXSum = mCalcSum(lXCoords, 1);
		float lYSum = mCalcSum(lYCoords, 1);

		float lNumerator = lXCoords.size() * lXYSum - (lXSum * lYSum);

		float lXSquaredSum = mCalcSum(lXCoords, 2);
		float lYSquaredSum = mCalcSum(lYCoords, 2);

		float lDenom1 = (float) (lXCoords.size() * lXSquaredSum - Math.pow(lXSum, 2));
		float lDenom2 = (float) (lYCoords.size() * lYSquaredSum - Math.pow(lYSum, 2));

		float lDenominator = (float) Math.sqrt(lDenom1 * lDenom2);

		float lCorrelation = lNumerator / lDenominator;

		mDisplayCorrelation(lXCoords, lYCoords, lCorrelation);
		if (pIsExcel)
		{
			mDisplayCorrelationAnalyse(lCorrelation);
		}
	}

	private static void mCalculateVariance(String lArgs) throws NumberFormatException, IOException
	{
		BufferedReader lBr = null;
		try
		{
			lBr = new BufferedReader(new FileReader(lArgs));

			String lLine;
			List<Float> lList = new ArrayList<Float>();
			while ((lLine = lBr.readLine()) != null)
			{
				lList.add(Float.parseFloat(lLine));
			}
			lBr.close();

			double lMoyenne = mCalcMoyenne(lList);
			double lDistanceSum = 0;
			int lSize = lList.size();
			for (Float lVal : lList)
			{
				lDistanceSum += mCalcDistance(lVal, lMoyenne);
			}
			double lVariance = mCalcVariance(lSize, lDistanceSum);
			double lEcart = Math.sqrt(lVariance);

			mDisplayVariance(lList, lMoyenne, lVariance, lEcart);
		}
		catch (FileNotFoundException e)
		{
			System.out.println(e);
		}
	}

	private static void mLectureExcel(String pArgs, List<Float> pXCoords, List<Float> pYCoords) throws IllegalStateException
	{
		try
		{
			FileInputStream lFile = null;
			try
			{
				lFile = new FileInputStream(new File(pArgs));

				XSSFWorkbook lWb = new XSSFWorkbook(lFile);
				XSSFSheet lSheet = lWb.getSheetAt(0);
				lWb.close();

				XSSFRow lRow;
				XSSFCell lCell;

				int lRows;
				lRows = lSheet.getPhysicalNumberOfRows();

				int lCols = 0;
				int lTmp = 0;

				for (int lIndex = 0; lIndex < 10 || lIndex < lRows; lIndex++)
				{
					lRow = lSheet.getRow(lIndex);
					if (lRow != null)
					{
						lTmp = lSheet.getRow(lIndex).getPhysicalNumberOfCells();
						if (lTmp > lCols)
						{
							lCols = lTmp;
						}
					}
				}

				float lSomme = 0;

				// Sauter les lignes (Titres)
				for (int lRowsIndex = 2; lRowsIndex < lRows; lRowsIndex++)
				{
					lRow = lSheet.getRow(lRowsIndex);
					if (lRow != null)
					{
						// Sauter la premiere colonne (noms)
						for (int lColsIndex = 1; lColsIndex < lCols; lColsIndex++)
						{
							lCell = lRow.getCell(lColsIndex);
							if (lCell != null)
							{
								if (lColsIndex < lCols - 1)
								{
									try
									{
										lSomme += lCell.getNumericCellValue();
									}
									catch (IllegalStateException e)
									{
										System.out.println(e + " on cell " + lCell.getReference());
									}
								}
								else
								{
									pYCoords.add((float) lCell.getNumericCellValue());
								}
							}
						}
					}
					pXCoords.add(lSomme);
					lSomme = 0;
				}
			}
			catch (FileNotFoundException e)
			{
				System.out.println(e);
			}
		}
		catch (Exception lException)
		{
			lException.printStackTrace();
		}
	}

	private static void mLectureTxt(String pArgs, List<Float> pXCoords, List<Float> pYCoords)
	{
		String lLine;
		String[] lCoords;

		try
		{
			BufferedReader lBr = null;
			try
			{
				lBr = new BufferedReader(new FileReader(pArgs));
				while ((lLine = lBr.readLine()) != null)
				{
					lLine = lLine.replaceAll(",", ".");
					lCoords = lLine.split("\\s");

					pXCoords.add(Float.parseFloat(lCoords[0]));
					pYCoords.add(Float.parseFloat(lCoords[1]));
				}
				lBr.close();
			}
			catch (FileNotFoundException e)
			{
				System.out.println(e);
			}
		}
		catch (NumberFormatException | IOException lException)
		{
			System.out.println("Erreur: " + lException.getMessage());
		}
	}

	private static void mDisplayIntervalle(int pY, float pItvl90, float lInf90, float lSup90, float pItvl70, float lInf70, float lSup70)
	{
		System.out.println("========================================");
		System.out.println("Intervalle :: " + pY + " LOC");
		System.out.println("90%: [ " + pY + "-" + (int) pItvl90 + ", " + pY + "+" + (int) pItvl90 + " ] = [ " + (int) lInf90 + ", " + (int) lSup90 + " ]");
		System.out.println("70%: [ " + pY + "-" + (int) pItvl70 + ", " + pY + "+" + (int) pItvl70 + " ] = [ " + (int) lInf70 + ", " + (int) lSup70 + " ]");
	}

	private static float mCalcInterval(float pT, float pEcType, float pXMoy)
	{
		float lP1 = 1;
		float lP2 = (float) 1 / aXCoords.size();

		float lP3Num = (float) Math.pow(XK - pXMoy, 2);
		float lP3Den = 0;

		for (int i = 0; i < aXCoords.size(); i++)
		{
			lP3Den += (float) Math.pow(aXCoords.get(i) - pXMoy, 2);
		}

		float lP3 = lP3Num / lP3Den;
		float lSqurt = (float) Math.sqrt(lP1 + lP2 + lP3);

		float lItvl = pT * pEcType * lSqurt;
		return lItvl;
	}

	private static float mCalcSumVar(List<Float> pXCoords, List<Float> pYCoords, float pPente, float pCste)
	{
		float lSomme = 0;
		for (int i = 0; i < pXCoords.size(); i++)
		{
			float lVal = pYCoords.get(i) - pCste - pPente * pXCoords.get(i);
			lSomme += Math.pow(lVal, 2);
		}
		float lFact = (float) 1 / (pXCoords.size() - 2);
		float lVar = lFact * lSomme;
		return lVar;
	}

	private static float mCalcSum(List<Float> pList, int pExp)
	{
		float lSum = 0;
		for (float lVal : pList)
		{
			lSum += (float) Math.pow(lVal, pExp);
		}
		return lSum;
	}

	private static float mCalcSum(List<Float> pLs1, List<Float> pLs2)
	{
		float lSum = 0;
		for (int lIndex = 0; lIndex < pLs1.size(); lIndex++)
		{
			lSum += pLs1.get(lIndex) * pLs2.get(lIndex);
		}
		return lSum;
	}

	private static void mDisplayVariance(List<Float> pList, double pMoyenne, double pVariance, double pEcart)
	{
		System.out.println("Variance");
		System.out.println(pList.size() + " donnees: " + pList.toString());
		System.out.println("Moyenne: " + pMoyenne);
		System.out.println("Variance: " + pVariance);
		System.out.println("Ecart-type: " + pEcart);
	}

	private static void mDisplayCorrelation(List<Float> pXCoords, List<Float> pYCoords, float pCorrelation)
	{
		System.out.println("========================================");
		System.out.println("Correlation");

		System.out.println("x: " + pXCoords);
		System.out.println("y: " + pYCoords);
		System.out.println("r^1 = " + pCorrelation);
		System.out.println("r^2 = " + Math.pow(pCorrelation, 2));
	}

	private static void mDisplayCorrelationAnalyse(float pCorrelation)
	{
		String lIntro = "TP5: il existe une correlation ";
		String lConcl = " entre l'effort consacree aux etudes et la note d'examen.";

		int lCorrlIndex = 0;
		String[] lCorrlArray = { "faible", "moderee", "forte", "parfaite" };

		if (pCorrelation < CORR_FAIBLE)
		{
			lCorrlIndex = 0;
		}
		else if (pCorrelation < CORR_MODERE)
		{
			lCorrlIndex = 1;
		}
		else if (pCorrelation < CORR_FORTE)
		{
			lCorrlIndex = 2;
		}
		else if (pCorrelation < CORR_PARFAIT)
		{
			lCorrlIndex = 3;
		}

		System.out.println(lIntro + lCorrlArray[lCorrlIndex] + lConcl);
	}

	private static void mDisplayRegression(List<Float> pXCoords, List<Float> pYCoords, float pPente, float pConstante)
	{
		System.out.println("========================================");
		System.out.println("Regression");

		System.out.println("x: " + pXCoords);
		System.out.println("y: " + pYCoords);
		System.out.println("b1 = " + pPente);
		System.out.println("b0 = " + pConstante);
	}

	private static double mCalcMoyenne(List<Float> pListe)
	{
		float lSum = mCalcSum(pListe, 1);
		int lSize = pListe.size();
		return 1.0 / lSize * lSum;
	}

	private static double mCalcDistance(Float pVal, double pMoy)
	{
		double lDistance = pVal - pMoy;
		return Math.pow(lDistance, 2);
	}

	private static double mCalcVariance(int pSize, double pDistanceSum)
	{
		double lValue = 1.0 / (pSize - 1.0);
		return lValue * pDistanceSum;
	}
}
